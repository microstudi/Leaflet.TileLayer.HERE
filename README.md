
# Leaflet.TileLayer.HERE

Displays [map tiles from HERE maps](https://developer.here.com/rest-apis/documentation/enterprise-map-tile/topics/quick-start.html) in your Leaflet map.


See the [live demo](http://ivansanchez.gitlab.io/Leaflet.TileLayer.HERE/demo.html).

## Usage

```
L.tileLayer.here({apiKey: 'abcde'}).addTo(map);
```

The following options are available:

| option       | type    | default        |                                                                            |
| ------------ | ------- | -------------- | -------------------------------------------------------------------------- |
| `scheme`     | String  | `'normal.day'` | The "map scheme", as documented in the HERE API.                           |
| `resource`   | String  | `'maptile'`    | The "map resource", as documented in the HERE API.                         |
| `mapId`      | String  | `'newest'`     | Version of the map tiles to be used, or a hash of an unique map            |
| `format`     | String  | `'png8'`       | Image format to be used (`png8`, `png`, or `jpg`)                          |
| `apiKey`      | String  | none           | Required option. The `apiKey` provided as part of the HERE credentials     |
| `appId`      | String  | none           | Obsolete. The `app_id` is the legacy method for HERE credentials, kept for compatibility. You should use apiKey instead.     |
| `appCode`    | String  | none           | Obsolete. The `app_code` is the legacy method for HERE credentials, kept for compatibility. You should use apiKey instead.   |




## Legalese

----------------------------------------------------------------------------

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

----------------------------------------------------------------------------


